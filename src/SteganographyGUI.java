import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class SteganographyGUI extends JPanel implements ActionListener {

    private JPanel panel1;
    private JPanel panel2;
    private JButton loadImageButton;
    private JButton decodeImageButton;
    private JList<String> list1;
    private JProgressBar progressBar1;
    private JTextArea textArea1;
    private JButton encodeTextButton;
    private JButton encodeFilesButton;
    private JLabel picLabel;
    private JFileChooser fc;

    private String imagePath;
    private SteganographicImage image;

    public void actionPerformed(ActionEvent e){
        if(e.getSource() == loadImageButton){
            fc = new JFileChooser();
            fc.setMultiSelectionEnabled(false);
            int returnVal = fc.showOpenDialog(SteganographyGUI.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                imagePath = file.getAbsolutePath();
                try{
                    image = SteganographicImage.loadFromFile(imagePath);
                    picLabel.setIcon(new ImageIcon(image));
                    picLabel.setText("");
                }catch(IOException IOe){
                    picLabel.setText("Wrong path");
                }

            }
        }
        if(e.getSource() == decodeImageButton){
            if(image != null) {
                String message = image.getMessage();
                String[] attachments = image.listAttachments();

                textArea1.setText(message);
                for (String a : attachments) {
                    ((DefaultListModel) list1.getModel()).addElement(a);
                }
            }
        }
        if(e.getSource() == encodeTextButton){
            if(!textArea1.getText().isEmpty() && textArea1.getText() != null && image != null){
                try{
                    image.setMessage(textArea1.getText());
                }catch (SteganographicImage.SteganographicImageException SIe){
                    System.err.println("not enough space...");
                }
            }
        }
        if(e.getSource() == encodeFilesButton && image != null){
            fc = new JFileChooser();
            fc.setMultiSelectionEnabled(true);
            int returnVal = fc.showOpenDialog(SteganographyGUI.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File[] files = fc.getSelectedFiles();
                for (File f : files){
                    try{
                        image.addAttachment(f.getAbsolutePath());
                    }catch (IOException IOe){
                        System.err.println("file does not exist");
                    }catch (SteganographicImage.SteganographicImageException ste){
                        System.err.println("Not enough space");
                    }finally {
                        //TODO update ui
                    }
                }
            }
        }
    }


    public SteganographyGUI(){
        super(new FlowLayout());

        panel1 = new JPanel();
        panel1.setSize(1000, 800);
        panel2 = new JPanel();
        panel2.setSize(280, 800);

        loadImageButton = new JButton("Load Image");
        decodeImageButton = new JButton("Decode Image");
        encodeFilesButton = new JButton("Encode Files");
        encodeTextButton = new JButton("Encode Text");
        picLabel = new JLabel("TEST");
        textArea1 = new JTextArea();
        progressBar1 = new JProgressBar();

        loadImageButton.addActionListener(this);
        decodeImageButton.addActionListener(this);
        encodeFilesButton.addActionListener(this);
        encodeTextButton.addActionListener(this);

        list1 = new JList<String>(new DefaultListModel<String>());
        ((DefaultListModel) list1.getModel()).addElement("AAAAAAAAAA");

        textArea1.setLineWrap(true);
        textArea1.setToolTipText("Text to encode/Decoded text.");
        list1.setToolTipText("List of encoded files.");

        panel1.add(picLabel);
        panel2.add(loadImageButton);
        panel2.add(decodeImageButton);
        panel2.add(encodeFilesButton);
        panel2.add(encodeTextButton);
        panel2.add(textArea1);
        panel2.add(list1);
        panel2.add(progressBar1);

        //if(!imagePath.isEmpty()){
        //    picLabel.setIcon(new ImageIcon(imagePath));
        //}else{
        //    picLabel.setText("no image");
        //}

        add(panel1);
        add(panel2);
    }

    public static void createAndShowGUI(){
        JFrame frame = new JFrame("Steganography");
        frame.setSize(1280, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add content to the window.
        frame.add(new SteganographyGUI());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        createAndShowGUI();
    }
}
